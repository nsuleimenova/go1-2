package main

import (
	"fmt"
	"time"
)

var chanForTweets chan *Tweet

func producer(stream Stream) {
	for {
		tweet, err := stream.Next()
		if err == ErrEOF {
			close(chanForTweets)
			return
		}
		chanForTweets <- tweet
	}
}



func consumer() {
	for t := range chanForTweets {
		if t.IsTalkingAboutGo() {
			fmt.Println(t.Username, "\ttweets about golang")
		} else {
			fmt.Println(t.Username, "\tdoes not tweet about golang")
		}
	}
}


func main() {
	start := time.Now()
	stream := GetMockStream()

	chanForTweets = make(chan *Tweet)
	go producer(stream)
	consumer()
	
	fmt.Printf("Process took %s\n", time.Since(start))
}
